#!/bin/bash

TZDATA_REGION=America/Los_Angeles # timezone in case tex files have timestamps
PROCS=$(($(nproc) + 1)) # how many cores to use when building

ln -fs /usr/share/zoneinfo/$TZDATA_REGION /etc/localtime
make -j $PROCS
