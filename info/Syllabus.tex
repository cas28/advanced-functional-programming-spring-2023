\documentclass{article}

\usepackage{parskip}
\usepackage{hyperref}
\usepackage[margin=0.75in]{geometry}
\usepackage{hhline}

\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=blue,
}

\urlstyle{same}

\title{CS 410/510 Advanced Functional Programming}
\author{Instructor: Katie Casamento}
\date{Spring 2023 Syllabus}

\begin{document}
\maketitle

\subsection*{Course description}

Undergraduate course descriptions can be found at \url{https://www.pdx.edu/computer-science/undergraduate-courses}.

Graduate course descriptions can be found at \url{https://www.pdx.edu/computer-science/graduate-courses}.

\subsection*{Academic misconduct}

  Review PSU's rules at \url{https://www.pdx.edu/dos/academic-misconduct}.

  This is not an exhaustive list, but here are some relevant principles:

    \begin{itemize}
      \item Always properly cite any content (code or writing) that someone else created when you include it in your work.
      \item Never claim that you created content that someone else created; always cite all collaborators on any group work you participated in.
      \item Obey intellectual property laws and ethics: pay attention to LICENSE files!
    \end{itemize}

\subsection*{Repository}

  Most of the material for this course will be tracked in a GitLab repository at \url{https://gitlab.cecs.pdx.edu/cas28/advanced-functional-programming-spring-2023}. This will let you see the history of any changes that I may make to the course material throughout the quarter.

\subsection*{Contact information}

Please \href{mailto:cas28@pdx.edu,theod@pdx.edu}{email both of us} if you have a question!

My email address is \href{mailto:cas28@pdx.edu}{cas28@pdx.edu}.

Ted Cooper is our TA for this course; Ted's email address is
\href{mailto:theod@pdx.edu}{theod@pdx.edu}.  Ted's office hours are Wednesdays
at 11am, in the fishbowl or \url{https://pdx.zoom.us/j/88384842328}.  If it's
busy, folks there in person will get help first.

\subsection*{Zulip forum}

We will be using the CAT's Zulip server for course discussion. The URL for the course discussion forum is \url{https://fishbowl.zulip.cs.pdx.edu/#narrow/stream/184-afp-spring-2023}.

\subsection*{Office hours}

I will be holding office hours 6:40-7:40pm on Thursdays, in FAB 115D or at \url{https://pdx.zoom.us/j/89789274086}.

Other times may be available by email appointment, for individuals or small groups.

\subsection*{Live lecture}

Live lectures are 6:40-8:30 on Tuesdays and Thursdays, in EB 92 or at \url{https://pdx.zoom.us/j/87395270399}.

Attendance is not graded, but is strongly encouraged.

\subsection*{Lecture recordings}

A recording of each live lecture will be made available on Canvas within 48 hours of the live lecture time.

The lecture recordings will only include me; the students in the classroom will not be recorded.

\subsection*{Software}

Lectures will use mostly use Haskell, and possibly other languages.

I will walk through setup instructions for each language I use in lecture.

\subsection*{Assignments}

There will be three (relatively large) assignments: one in a language of your choice, and two in Haskell.\\
Assignments will generally cover the material we cover in lecture.

Assignments are individual work. Do not share your answers to an assignment with other students in this course.

You may use any tools and consult any sources while you work, as long as you cite your sources clearly.\\
(With one exception: you may not consult the answers of another student in this course.)

Assignment instructions will be posted on Canvas, and your answers will be submitted to Canvas.\\
The assignment instructions will include specific submission instructions.

\subsection*{Course projects}

Graduate students (enrolled in CS 510) are required to complete at least one individual course project \textbf{in addition} to the course assignments.

Any student may also complete individual course projects \textbf{instead} of one or more assignments, if you get it approved with me ahead of time.

To complete a course project, you must send me a project proposal over email detailing what you plan to work on, \textbf{at least two weeks before you plan to submit the completed project}. I'll try to respond to these emails as quickly as possible.

Projects can be code projects or textbook-style exercise sets; I can give recommendations for both. Project code can be in any language of your choice.

If you want to do projects instead of the assignments, a single course-long project may count for all three assignment grades. To take this option, you must submit a proposal for your project \textbf{by the end of week 3}.

No group projects, but unlike the assignments, feel free to share any of the code of your projects with anyone,\\
as long as they're not working on the exact same project for this course.

Projects may involve any code you write that you have permission to share with me.\\
You're welcome to use code you're working on for other courses or your job as long as you have permission.

\subsection*{Grading}

Each assignment will be weighted equally in the final grade. The individual course project for graduate students will count as a fourth assignment.

If you meaningfully complete required coursework, you'll do fine in this course. I think the students in this course are here because they genuinely want to learn the material, and this isn't an important prerequisite for any other course or qualification or anything, so I don't see any reason to grade harshly!

Projects will be assessed based on \textbf{progress}, not \textbf{completion}. This is an important difference!\\
Don't worry about finishing all of your goals, just make sure that you're making progress on them before the deadline.

\end{document}
