from os import path, symlink
from pathlib import Path
from sys import argv, exit

from canvasapi import Canvas

course = Canvas("https://canvas.pdx.edu", argv[1]).get_course(68274)

Path("deploy").mkdir(exist_ok=True)

basename = lambda filename: str(filename).split('/')[-1].split('.')[0]


##########
## info ##
##########

info_module = course.get_module(392605)
already_uploaded_info = {item.title for item in info_module.get_module_items()}

for filename in Path("info").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="info")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)

  title = basename(filename)
  if title not in already_uploaded_info:
    info_module.create_module_item({
      "title": title,
      "type": "File",
      "content_id": upload["id"]
    })


###########################
## haskell review slides ##
###########################

info_module = course.get_module(392695)
already_uploaded_info = {item.title for item in info_module.get_module_items()}

for filename in Path("review").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="info")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)

  title = basename(filename)
  if title not in already_uploaded_info:
    info_module.create_module_item({
      "title": title,
      "type": "File",
      "content_id": upload["id"]
    })


#################
## assignments ##
#################

for filename in Path("hw").glob("*.pdf"):
  success, upload = course.upload(filename, parent_folder_path="hw")
  if not success:
    print("failed to upload {}".format(filename))
    exit(-1)


# ###########
# ## notes ##
# ###########
# 
# notes_module = course.get_module(331692)
# already_uploaded_notes = {item.title for item in notes_module.get_module_items()}
# Path("deploy/notes").mkdir(exist_ok=True)
# 
# # expects input that matches "notes/*.pdf"
# # filename_index = lambda filename: int(basename(filename))
# filename_index = lambda filename: int(str(filename).split('/')[-1].split('.')[0])
# 
# offset = 0
# for input_filename in sorted(Path("notes").glob("*.pdf"), key=filename_index):
#   title = open(str(input_filename)[:-3] + "ttl").read()
#   output_filename = "deploy/notes/" + title + ".pdf"
#   symlink(path.abspath(input_filename), path.abspath(output_filename))
# 
#   success, upload = course.upload(output_filename, parent_folder_path="notes")
#   if not success:
#     print("failed to upload {} ({})".format(input_filename, title))
#     exit(-1)
# 
#   index = filename_index(input_filename)
#   if title in already_uploaded_notes:
#     offset += 1
#   else:
#     notes_module.create_module_item({
#       "title": title,
#       "position": index + offset,
#       "type": "File",
#       "content_id": upload["id"]
#     })
# 
#   code_filename = "notes/code" + str(index) + ".agdacode"
#   code_title = "code" + str(index) + ".agda"
#   if path.exists(code_filename):
#     symlink(path.abspath(code_filename), path.abspath("deploy/notes/" + code_title))
# 
#     code_success, code_upload = course.upload("deploy/notes/" + code_title, parent_folder_path="notes")
#     if not code_success:
#       print("failed to upload {} ({})".format(code_filename, code_title))
#       exit(-1)
# 
#     if code_title not in already_uploaded_notes:
#       notes_module.create_module_item({
#         "title": code_title,
#         "position": index + 1,
#         "type": "File",
#         "indent": 1,
#         "content_id": code_upload["id"]
#       })
#       offset += 1
