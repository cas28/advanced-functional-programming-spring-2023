# This build setup is nasty, but I've never seen a LaTeX build setup that isn't...

# This Makefile is written for a bash-like shell environment with a TexLive
# distribution installed. On Windows, it might be easiest to build with WSL.

# Building the output for deployment requires Agda, XeTeX, Python 3, and xindy.
# If any of them is installed with a different executable name, or installed
# somewhere that isn't on your system PATH, you can set the path to each
# executable here.
AGDA=agda
XETEX=xelatex
PYTHON=python3
XINDY=xindy



#########
## all ##
#########

infos := $(wildcard info/*.tex)
hws := $(wildcard hw/*.tex)
notes := $(wildcard notes/[0-9].tex) $(wildcard notes/[0-9][0-9].tex)

all: \
	$(infos:.tex=.pdf.done) \
	$(hws:.tex=.pdf.done) \
	$(notes:.tex=.pdf.done) $(notes:.tex=.ttl) \



##########
## info ##
##########


info/%.pdf info/%.pdf.done: info/%.tex
# LaTeX needs two compilation runs to get the table of contents right.
# This can leave behind a .pdf file when the first compilation succeeds and the
# second fails, which confuses Make, so the .pdf.done files give it something
# separate to hang a timestamp on when a build actually succeeds.
	$(XETEX) -interaction=nonstopmode --output-directory=info "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=info "$<"
	touch "$@" # create .pdf.done file if that was the target



##############
## homework ##
##############


hw/%.pdf hw/%.pdf.done: hw/%.tex
	$(XETEX) -interaction=nonstopmode --output-directory=hw "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=hw "$<"
	touch "$@"



###########
## notes ##
###########


notes/%.ttl: notes/%.tex notes/gettitle.sh
# gettitle.sh extracts the title of each LaTeX document and stores it in a .ttl
# file so that the deployment script can use it to name things later.
	echo -n "$(shell notes/gettitle.sh "$<")"> $(basename $<).ttl


notes/%.pdf notes/%.pdf.done: notes/%.tex
	$(XETEX) -interaction=nonstopmode --output-directory=notes "$<"
	$(XETEX) -interaction=nonstopmode --output-directory=notes "$<"
	touch "$@" # create .pdf.done file if that was the target



# LaTeX is so messy!
.PHONY: clean clean-info clean-hw clean-notes
clean: clean-info clean-hw clean-notes

clean-info:
	cd info; \
	rm -f *.aux *.glg *.glo *.gls *.log *.out *.pdf *.ptb *.toc *.xdy; \
	rm -f *.done

clean-hw:
	cd hw; \
	rm -f *.aux *.glg *.glo *.gls *.log *.out *.pdf *.ptb *.toc *.xdy; \
	rm -f *.done

clean-notes:
	cd notes; \
	rm -f *.aux *.glg *.glo *.gls *.glt *.log *.out *.pdf *.ptb *.toc *.ttl *.xdy; \
	rm -f *.done
