\documentclass[parskip]{scrartcl}
\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}
\usepackage{hyperref}
\usepackage{bookmark}
\usepackage{xcolor}
\usepackage{enumitem}

\hypersetup{%
  colorlinks=true,
  linkcolor=blue,
  anchorcolor=blue,
  urlcolor=blue,
}

\subject{CS 410/510 Advanced Functional Programming}
\author{Katie Casamento}
\date{Spring 2023}
\publishers{Portland State University}

\title{Assignment 3}
\subtitle{Functional application development}

\begin{document}

\maketitle

\section{Introduction}

This final assignment is an open-ended project based on the topics that we've been covering for the past several weeks. Your overall goal in this assignment is to implement a variant of the game of tic-tac-toe, using Haskell with the \href{https://hackage.haskell.org/package/brick}{\texttt{brick}} library that we saw in assignment 2.

\section{Program requirements}

  These are the requirements for how your program should behave from the user's perspective.

  \subsection{Command-line arguments}

    Your program must support two command-line arguments, which the user should be able to pass in either order:

    \begin{itemize}[noitemsep]
      \item \texttt{-s} to control the size of the board, which must be a positive (nonzero) integer
      \item \texttt{-p} to control which player goes first, which must be either \texttt{X} or \texttt{O}
    \end{itemize}

    The board should always be square, so for example \texttt{-s 15} indicates a $15 \times 15$ board. If the user does not explicitly pass an argument, the default argument values should be \texttt{-s 3} and \texttt{-p X}.

  \subsection{User interface}

    While the game is being played, the user interface should use a \texttt{brick} table view similar to the Minesweeper grid in assignment 2. An empty cell should show an empty space, and a cell with an \texttt{X} or \texttt{O} in it should show the corresponding letter.

    On each player's turn, the user should be able to select a cell on the board with the arrow keys and the Enter key. Each player should alternate turns until the game has ended (one player has won or the game has ended in a tie).

    If the user tries to select a cell that already contains an \texttt{X} or \texttt{O}, the input should just be ignored.

    When the game ends, your program should exit the table view and print a message to the console saying \texttt{X wins}, \texttt{O wins}, or \texttt{tie game}.

  \subsection{Game rules}

    On an $n{\times}n$ board, there are $2n{+}2$ different ways for a player to win:

    \begin{itemize}[noitemsep]
      \item $n$ rows
      \item $n$ columns
      \item 2 diagonals
    \end{itemize}

    Note that there are always only 2 diagonals regardless of how big the board is: for example, this board below does \textbf{not} count as a win for \texttt{O} because the line doesn't go from one corner to the opposite corner.

    \begin{center}
      \begin{tabular}{| p{0.5em} | p{0.5em} | p{0.5em} | p{0.5em} | p{0.5em} |}
        \hline
        \texttt{X} & \texttt{O} & & & \\
        \hline
        & \texttt{X} & \texttt{O} & & \\
        \hline
        & & & \texttt{O} & \texttt{X} \\
        \hline
        & & & & \texttt{O} \\
        \hline
        & & & & \\
        \hline
      \end{tabular}
    \end{center}

    In other words, to win on an $n{\times}n$ board, a player always needs a line of exactly $n$ cells.

    The game should end immediately after a player makes a winning move. If neither player wins, the game should end with a tie after the last empty cell is filled with an \texttt{X} or \texttt{O}.


\section{Code requirements}

  These are the requirements for how you should write the code of your program.

  \subsection{Academic honesty requirements}

    Breaking these requirements will count as an academic honesty violation.

    \begin{itemize}
      \item \textbf{You must not submit any shared code that another student is also submitting for this assignment}. You are encouraged to \textbf{discuss the concepts} of the assignment and \textbf{share sample code that you do not intend to submit}, but you \textbf{must not share code that you are going to submit}.

      \item If you take any code from the internet or any other source, you must \textbf{cite where it came from} and \textbf{comply with its license}. If the code doesn't come with a license, by default that means the code cannot be legally copied or shared.
    \end{itemize}

  \subsection{Grade requirements}

    Breaking these requirements will cause deductions to your assignment grade.

    \begin{itemize}
      \item Your code must compile and run with at least one GHC version that is 9.2.7 or greater. (You don't need to test for compatibility with multiple GHC versions, just make sure you're not using anything older than 9.2.7.)

      \item You must not write any calls to the functions \texttt{head}, \texttt{tail}, or \texttt{fromJust}, or the \texttt{!} or \texttt{!!} operators, or any other functions that have any possibility of a runtime crash.

      \item You must not write any calls to the \texttt{error} function or any uses of \texttt{undefined}.

      \item You must not write any module imports that include the word \texttt{Unsafe}.

      \item All of the code that you write must run in finite time for all possible inputs. You will not be graded on performance, but your code must not go into any infinite recursion.
    \end{itemize}

    Apart from these requirements, you have total freedom in how you write your code: you may import any packages, set any compiler flags, and use any code style.


\section{Getting started}

  I recommend starting with a copy of the assignment 2 codebase and modifying it for the needs of this project. (The assignment 2 license allows you to copy it!)

  I also recommend deeply reviewing the provided assignment 2 code as we work through the topic of monads in lecture. Focus in particular on the code that uses \texttt{IO} and \texttt{EventM}; the \texttt{State} code will be mostly irrelevant to this project.

  The \href{https://hackage.haskell.org/package/base-4.16.0.0/docs/Prelude.html}{\texttt{Prelude} documentation} explains everything that's imported automatically in a Haskell module, and you can find the documentation for all other built-in modules in the \href{https://hackage.haskell.org/package/base-4.16.0.0/docs/index.html}{\texttt{base} module listing}. You can also find documentation on Hackage for any third-party packages that you use, including \texttt{brick}.

  I will give a lot of advice for this project over the next several lectures, so make sure to keep up and review the videos when you forget things!

\end{document}
