-- This is the code that we covered in lecture on Monday of week 2, with added
-- comments and a bit of added code.

module Week2Monday where

-- The Temperature type that we saw in the previous lecture was an example of
-- how a type may have multiple constructors that each take different types of
-- arguments.

-- A generalization of this idea is that we can define a *recursive* type, with
-- "base case" constructors that do not take the type itself as an argument,
-- and "step case" constructors that do take the type itself as an argument.

-- The first example of this that we'll see is an immutable singly-linked list
-- of integers. This List type has two traditionally-named constructors, for
-- the "base case" and "step case" of building a list.
data List where
  Nil :: List                 -- empty list: has no contents
  Cons :: Int -> List -> List -- nonempty list: has a "head" and a "rest"
  deriving Show               -- (we'll talk more about this line soon)

-- The intuition for this List definition is that every singly-linked list is
-- either empty (length 0) or nonempty (length n+1 for some non-negative n).

-- The Nil constructor takes no arguments:
emptyList :: List
emptyList = Nil

-- The Cons constructor takes two arguments, in order:
-- * a "head" Int, the value of the first cell in the list
-- * a "rest" List, containing all the rest of the elements in the list
singleElementList :: List
singleElementList = Cons 1 Nil

threeElementList :: List
threeElementList = Cons 1 (Cons 2 (Cons 3 Nil))

-- In comparison to singly-linked list naming conventions in more traditional
-- languages, the "rest" argument is our "next pointer". If you're coming from
-- a language like C++ or Rust, though, note that these lists will be
-- garbage-collected, so we're not interacting with pointers directly.

-- Again, this is also an *immutable* singly-linked list type. We will see in a
-- future lecture how to define a *mutable* singly-linked list type in Haskell,
-- which is actually possible (despite common misconceptions about Haskell),
-- but requires a deeper understanding of how IO works in this setting.


-- doubleEach is a function which takes a List as input and returns a List as
-- output - a "modified copy" of the input list with each value doubled.
doubleEach :: List -> List
doubleEach Nil = Nil
doubleEach (Cons x xs) = Cons (2 * x) (doubleEach xs)

-- Breaking down that definition:

-- doubleEach Nil = Nil
--   There is nothing to double in an empty list: if the input list is empty,
--   then the output list should be empty.

-- doubleEach (Cons x xs) = Cons (2 * x) (doubleEach xs)
--   If the input list is nonempty, it has a "head" (x) and a "rest" (xs).
--   (We can choose any names we want: "x" and "xs" are just traditional.)
--   We double the head: 2 * x
--   We double the rest: doubleEach xs
--   Then we join those two results together into an output list with Cons.

-- If you open this code project in the interpreter (cabal repl) you can try
-- out the doubleEach function, for example:

--   doubleEach emptyList
--   doubleEach singleElementList
--   doubleEach (Cons 10 singleElementList)

-- To be precise, this is the sequence of steps in the evaluation of the
-- expression "doubleEach threeElementList":

--   doubleEach (Cons 1 (Cons 2 (Cons 3 Nil)))     -- x = 1, xs = Cons 2 (Cons 3 Nil)
--   = Cons (2 * 1) (doubleEach (Cons 2 (Cons 3 Nil)))
--   = Cons 2 (doubleEach (Cons 2 (Cons 3 Nil)))   -- x = 2, xs = Cons 3 Nil
--   = Cons 2 (Cons (2 * 2) (doubleEach (Cons 3 Nil)))
--   = Cons 2 (Cons 4 (doubleEach (Cons 3 Nil)))   -- x = 3, xs = Nil
--   = Cons 2 (Cons 4 (Cons (2 * 3) (doubleEach Nil)))
--   = Cons 2 (Cons 4 (Cons 6 (doubleEach Nil)))
--   = Cons 2 (Cons 4 (Cons 6 Nil))

-- What GHC actually does when executing this function call involves quite a
-- bit of automatic optimization, but at a logical level we are able to treat
-- the = sign in function definitions as *true* equality: each expression in
-- the above series of equations is, in a very meaningful sense, the *same*
-- as each other expression, except in how "finished" they are.


-- Let's briefly note that this style of recursive type definition is not only
-- useful for defining linked lists; it can be used to define nearly any
-- recursive data structure. (Cyclic data structures are a little tricky.)

-- For example, we could define a complete binary tree type with Chars at the
-- inner nodes and Ints at the leaves:
data BinTree where
  Leaf :: Int -> BinTree
  Node :: Char -> BinTree -> BinTree -> BinTree
  deriving Show

-- Or a 2-3 tree with Floats at the leaves, where each inner node has either
-- two or three children:
data TwoThree where
  Leaf23 :: Float -> TwoThree -- we can't reuse the name Leaf in the same module
  TwoNode :: TwoThree -> TwoThree -> TwoThree
  ThreeNode :: TwoThree -> TwoThree -> TwoThree -> TwoThree
  deriving Show

-- We can also define recursive functions over all these different recursive
-- types by specifying what to do in each base case, and what to do in each
-- recursive case - like for the List type. The length23 function calculates
-- how many nodes (including leaves) are in a TwoThree tree.
length23 :: TwoThree -> Int
length23 (Leaf23 _) = 1
length23 (TwoNode l r) = length23 l + length23 r
length23 (ThreeNode l m r) = length23 l + length23 m + length23 r


-- Back on the topic of lists: we would certainly like to avoid having to write
-- a bunch of different list types for lists of Ints, and lists of Chars, and
-- lists of Bools, etc. Ideally we want to define a "List of whatever".

-- This would involve *parametric polymorphism*: writing a definition that
-- works for any possible "element" type. We will see a little bit of this at
-- the end of these notes, and much more in future lectures.

-- First, let's demonstrate that Haskell actually already has a parametrically
-- polymorphic list type built in. Apart from some different notation, and the
-- property of parametric polymorphism, it behaves *exactly* like the List type
-- that we defined by hand above.

-- The built-in list type in Haskell has some shorthand notation that we will
-- introduce below. First, here is the *most explicit* way to write a list of
-- three Ints with the built-in list type.
exampleBuiltInList :: [Int] -- the type of lists of integers
exampleBuiltInList = 1 : (2 : (3 : []))

-- There are two *constructors* of the built-in list type:
-- * the empty list constructor, written "[]", corresponds to Nil
-- * the non-empty "cons" constructor, written "x : xs", corresponds to Cons

-- This means we can define functions over the built-in list type in the same
-- way that we did with our hand-written List type, keeping the notation
-- differences in mind. Compare this definition to doubleEach.
doubleEachBuiltIn :: [Int] -> [Int]
doubleEachBuiltIn [] = []
doubleEachBuiltIn (x : xs) = (2 * x) : (doubleEachBuiltIn xs)

-- The shorthand for the built-in list type looks like this:
--   [1, 2, 3]    is short for    1 : (2 : (3 : []))

-- It's important to remember that this notation is *shorthand*! For example,
-- if we call doubleEachBuiltIn with the argument [1,2,3], we are *actually*
-- calling it with 1 : (2 : (3 : [])), which is why the pattern-matching works.
-- (Try it in the interpreter yourself to verify that it works!)


-- One final note that we only had time to briefly introduce in this lecture,
-- demonstrating the parametrically polymorphic nature of the built-in list
-- type. We will explore this concept in much, much more depth soon.

-- Remember that when we define a *type* or *constructor* in Haskell, we are
-- **required** to give it a name beginning with an uppercase letter. Unlike
-- in most other programming languages, this is not just tradition but actually
-- something that matters to the compiler.

-- Parametric polymorphism involves the use of *type variables*, which are
-- type-level names that begin with a lowercase letter. Whenever we see a name
-- like "a" or "thing" or even "xYZ123" written as part of a *type* in Haskell,
-- it will be a *type variable*.

-- Closely related features in other languages include "generic types" in Java
-- and many languages influenced by it, and "templated types" in C++. In those
-- languages, it is traditional (although not required) for type variables to
-- begin with an uppercase letter, with "T" as a common type variable name.

-- One of Haskell's distinctive strengths as a language is the ability to use a
-- **lot** of parametric polymorphism in our programs, actually strictly more
-- than most mainstream languages have the features to support. This enables us
-- to achieve a **lot** of code reuse, so that well-written Haskell programs
-- are usually very non-repetitive compared to programs in other languages.
-- We'll get into this later with our discussion of higher-kinded types.

-- Here's a first example of what will be many. The lengthBuiltIn function
-- calculates the length of an input list, which may have any type of elements.
lengthBuiltIn :: forall a. [a] -> Int
lengthBuiltIn [] = 0
lengthBuiltIn (_ : xs) = 1 + lengthBuiltIn xs

-- Breaking down that definition:

-- forall a. [a] -> Int
--   This whole thing is a *type*: it is the type of lengthBuiltIn. We might
--   read it as "for any element type 'a', lengthBuiltIn can take a list of 'a'
--   values and return an Int".
--
-- forall a.
--   This part is saying "in the rest of this type, 'a' is a type variable".
--   This is technically optional; Haskell knows that 'a' must be a type
--   variable, because it begins with a lowercase letter.
--
--   We will see some code patterns later where the "forall" is not optional,
--   so it's good to get used to it now. It's also helpful when learning the
--   language just to be very explicit about what's happening. The scope of the
--   forall is only this type: in other types elsewhere in this file, this
--   particular declaration of 'a' is not available.

-- [a] -> Int
--   This is the type of functions that take in a list of 'a' values and return
--   an Int. This is all we actually need to write if we want to write out the
--   type of lengthBuiltIn, since the "forall a." is optional. For the same
--   reason, this is the type we see if we run ":t lengthBuiltIn" in the
--   interpreter.

-- In the notation of a language like Java, we might write the signature of
-- lengthBuiltIn like this:
--   int lengthBuiltIn<T>(List<T> list)

-- The "forall a." in the Haskell type corresponds to the **first** use of
-- "<T>" in the Java type. In most languages with Java-style generics, this
-- part is not optional.


-- Note that forall is a contract that GHC strictly enforces: if we declare
-- that our function can take in a list with *any* element type, we are
-- required to define the function in a way that makes sense for *any* element
-- type. This is a very restrictive limitation, but it does allow for many
-- polymorphic functions which "rearrange" elements within a data structure.

-- To illustrate this point, here's a function to reverse any input list. Note
-- that the type of elements in the output must be the same as in the input,
-- because the same type variable is used in both parts of the type. The
-- Haskell typechecker enforces this restriction.
reverseBuiltIn :: forall a. [a] -> [a]
reverseBuiltIn [] = []
-- reverseBuiltIn (x : xs) = reverseBuiltIn xs ++ (x : [])
reverseBuiltIn (x : xs) = reverseBuiltIn xs ++ [x]

-- The ++ operator is used to concatenate two lists together. The two
-- definitions of the "cons" case in reverseBuiltIn demonstrate both the
-- explicit notation and the shorthand for a single-element list containing
-- only 'x'.


-- Finally, note that each **individual** list in Haskell can only have a
-- **single** element type - it is not valid to have a list containing both
-- Ints and Bools, for example. The lengthBuiltIn and reverseBuiltIn functions
-- each work on lists of Ints, and lists of Bools, and lists of lists of Ints,
-- etc., but the built-in list type does not allow "mixed-type" lists.

-- For people coming from languages like JavaScript, Python, or Lisp, a common
-- Haskell question is "what do I do when I want a mixed-type list?". This is a
-- very reasonable question, but it's based on a different notion of "types"
-- than we have in this setting. There are broadly two answers to the question,
-- which can both be summarized as "find a way to describe all the elements
-- with a single type".

-- The simplest and most direct way to have a "mixed-type list" in Haskell
-- applies when you have a specific finite set of types that might appear in
-- your list. For example, you might want a "list of ints or bools".

-- This is one good use case for types like our Temperature example from the
-- previous lecture. The type [Temperature] (list of Temperature values) can be
-- thought of as a "list of ints or floats", but note that it is **actually**
-- just a list of Temperatures - a list with a single element type.

-- Less commonly, you might not know in advance all of the types that might
-- appear in your list; maybe your code is a library that users might call with
-- mixed-type lists containing a bunch of their own user-defined types.

-- The technique for this involves *existential types* and *typeclasses*, and
-- it's closely related to "interface types" in Java-like OOP languages and
-- "trait object types" in Rust. The idea is to have a single type of "values
-- that can be converted to strings" (for example), allowing us to have the
-- type of "lists of values that can be converted to strings". This will still
-- involve a single element type, though: we will have a type [StringableThing]
-- along with a function from any "stringable" value to a StringableThing.

-- We'll see an example or two once we cover existential types, but in almost
-- all cases when you want a "mixed-type list" you can get away with just
-- defining a type like Temperature to use as your element type, and that's a
-- simpler and more efficient solution whenever it's applicable.
