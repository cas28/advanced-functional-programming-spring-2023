# Contributors guide

I welcome forks and merge requests on this repository! If I accept your merge request, I'll make sure to credit you in future versions of this material.


## The build system

This repository is organized as a single monolithic `Makefile` project, which builds all of the PDF and Agda files that get deployed to Canvas. The `Makefile` is commented, so hopefully it's possible to understand what it's doing even though it's an annoyingly complex build process.

If you submit a merge request, it should automatically run the CI pipeline on your submission and let you know whether it builds successfully. If it doesn't, you might have to update the build setup script in the `ci-cd` directory to make sure that the CI/CD server has all the right dependencies installed. (It only pulls in a subset of `texlive` in order to save a bit of time on each build.)

Here are a couple things that might not be obvious about how the build system works:

- The `Makefile` is set up to be reasonably parallelizable with the `-j` argument to `make`, which saves a lot of time on each build if you have cores to spare.
- When a PDF file gets built by `make`, an empty file with the `.pdf.done` extension also gets created. This is explained in the Makefile comments, but it's basically just a hack to make sure a partial rebuild after a failed build works properly.


## The glossaries

Each notes file gets an automatically-generated glossary file with the extension `.gls`, created by `include/vocab.py`. This scans the `.tex` file for uses of `\vocab` and creates a glossary entry for each one that doesn't have an entry already.

Here are a couple things that might not be obvious about how the glossary system works:

- `\Vocab` is used for capitalizing the first letter of a glossary word. (This avoids duplicate entries in the glossary.)
- `\vocabs` and `\Vocabs` are used for pluralizing glossary words by adding a single 's' at the end. If you want to put a word in the glossary with a different pluralization, it has to be manually defined in `include/header.tex`. If that's not powerful enough, you can use `\vocabstyle{term}{text}` to render `text` as a glossary link to `term`.
- `\vocab` treats dashes as spaces, so if you want to put a term with a dash in the glossary, it needs to be manually defined in `include/header.tex`.
- `\vocab` recognizes the special prefix `sym-` and transforms it into a backslash (as a purely textual macro, not within the LaTeX engine). For example, `\vocab{sym-Gamma}` will render as `\Gamma` and include a glossary entry for that symbol.
